﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imitation_of_work
{
    class Server
    {
        public static void GetSomeInfoToServer(string inputValue, string inputKey)
        {
            //Console.WriteLine("Please enter your name within the next 5 seconds.");
            if (Reader.TryReadLine(out string request, 3000))
            {
                //в случае если пользователь захоте вывести на консоль данные полученые сервером
                Console.WriteLine($"Server received: {inputValue}");
            }

            string resultEncrypt = StringCipher.Encrypt(inputValue, inputKey);
            Soket.ImportEncryptInfoFromServerToBrowser(resultEncrypt, inputKey);
        }
    }
}
