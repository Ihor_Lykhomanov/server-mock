﻿using System;
using System.Collections.Generic;

namespace Imitation_of_work
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, string> elements = new Dictionary<string, string>();
            elements.Add("dog", "Siberian Husky");
            elements.Add("cat", "Maine Coon");
            elements.Add("parrot", "Large Cockatoos");
            while (true)
                foreach (var elem in elements)
                    Server.GetSomeInfoToServer(elem.Value, elem.Key);
        }
    }
}
