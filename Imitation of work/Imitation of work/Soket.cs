﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imitation_of_work
{
    class Soket
    {
        public static void ImportEncryptInfoFromServerToBrowser(string encryptInfo, string key)
        {
            Browser.GetSomeInfoToBrowser(encryptInfo, key);
        }
        public static void ImportDecryptInfoFromBrowserToServer(string decryptInfo, string key)
        {
            Server.GetSomeInfoToServer(decryptInfo, key);
        }
    }
}
