﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imitation_of_work
{
    class Browser
    {
        public static void GetSomeInfoToBrowser(string inputEncrypt, string inputKey)
        {
            string resultDecrypt = StringCipher.Decrypt(inputEncrypt, inputKey);
            //Console.WriteLine("Please enter your name within the next 5 seconds.");
            if (Reader.TryReadLine(out string request, 3000))
            {
                //в случае если пользователь захоте вывести на консоль данные полученые сервером
                Console.WriteLine($"Browser received: {resultDecrypt}");
            }
            
            //Soket.ImportDecryptInfoFromBrowserToServer(inputKey, resultDecrypt);
        }
    }
}
